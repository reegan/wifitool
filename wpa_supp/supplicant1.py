#!/usr/bin/python
"""
It is implemented by the wpa_supplicant dbus object
"""
import dbus
from .supplicant import Supplicant


class Supplicant1(Supplicant):

    """
    Main DBus interface object for wpa_supplicant.
    """

    def __init__(self):
        """
        It is registered dbus name, interface, object path
        """
        super(Supplicant1, self).__init__()
        self.wpas_dbus_interface = 'fi.w1.wpa_supplicant1'
        self.wpas_dbus_opath = '/fi/w1/wpa_supplicant1'
        self.making_method_calls()
        self.path = None
        self._loop = None

    def get_interface(self, ifname):
        """
        Returns a D-Bus path to an object related to an
        interface which wpa_supplicant already controls.
        :param ifname:
        :return: dbus path
        """
        try:
            self.path = self.iface.GetInterface(ifname)

        except dbus.DBusException, exc:
            if str(exc).startswith("fi.w1.wpa_supplicant1.InterfaceUnknown:"):
                print "Given interface is unknown."
            elif str(exc).startswith("fi.w1.wpa_supplicant1.UnknownError:"):
                print "Unknown Error"
        return self.path

    # def scan_signal(self, handler_function, signal_name=None,
    #                 dbus_interface=None, bus_name=None,
    #                 path=None, **keywords):
    #     """
    #     :param handler_function: function name
    #     :param signal_name: properties signal
    #     :param dbus_interface: dbus interface name
    #     :param bus_name: dbus bus name
    #     :param path: This object path
    #     :param keywords: it is optional
    #     """
    #     self.bus.add_signal_receiver(
    #         handler_function, signal_name, dbus_interface,
    #         bus_name, path, **keywords)
    #     self._loop = gobject.MainLoop()
    #     self._loop.run()


# if __name__ == "__main__":
#     wpasupplicant = Supplicant1()
#     wpasupplicant.making_method_calls()
#     print wpasupplicant.get_interface("wlan0")
