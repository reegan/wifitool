"""
wifitool - %s

usage: wifitool <sub-command> <interface>

Sub-commands:

  wifitool bss wlan0
"""

from __future__ import print_function
from collections import namedtuple

import sys
from . import bssproper
from . import version

CommandInfo = namedtuple("CommandInfo", "nargs module")


CMDS = {
    "bss": CommandInfo(nargs=0, module=bssproper)
}


def usage(status):
    """Prints usage message and quits."""

    print(__doc__ % version.__VERSION__)
    exit(status)


def main():
    """Main application entry point."""
    if len(sys.argv) < 3:
        usage(1)

    cmd = sys.argv[1]
    args = sys.argv[2:]

    if cmd in CMDS and len(args) >= CMDS[cmd].nargs:
        CMDS[cmd].module.main(*args)
    else:
        print("invalid command or incorrect arguments", file=sys.stderr)
        usage(1)


if __name__ == "__main__":
    main()
